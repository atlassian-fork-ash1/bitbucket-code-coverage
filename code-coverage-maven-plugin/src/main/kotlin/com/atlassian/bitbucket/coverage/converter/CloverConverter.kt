package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.XmlReader
import com.atlassian.bitbucket.coverage.clover.Coverage
import com.atlassian.bitbucket.coverage.clover.Line
import com.atlassian.bitbucket.coverage.clover.Package
import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

class CloverConverter : AbstractCoverageConverter() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(CloverConverter::class.java)
    }

    override fun convert(coverageFile: File, projectPath: Path): CommitCoverageEntity {
        val coverage: Coverage = XmlReader().read(Coverage::class.java, coverageFile)
        val sourceFileResolver = SourceFileResolver(projectPath)

        val files = mutableListOf<FileCoverageEntity?>()

        for (pkg in coverage.project.`package`) {
            logger.debug("Processing package {}", pkg.name)
            val packageCoverage: List<FileCoverageEntity?> = convertPackage(pkg, projectPath)
            files.addAll(packageCoverage)
        }

        val result = CommitCoverageEntity()
        result.files = files.filterNotNull()
        return result
    }

    fun convertPackage(pkg: Package, projectPath: Path): List<FileCoverageEntity?> {
        val result: MutableList<FileCoverageEntity?> = mutableListOf()

        for (file in pkg.file) {
            val fileCoverage = convertFile(file, projectPath)
            result.add(fileCoverage)
        }

        return result
    }

    fun convertFile(file: com.atlassian.bitbucket.coverage.clover.File, projectPath: Path): FileCoverageEntity? {
        val fileCoverageMap = emptyCoverageMap()

        // type="cond" lines duplicate the coverage of the type="stmt" line that came just before
        var previousLine: Line? = null
        for (line in file.line) {
            if (previousLine != null && previousLine.num != line.num) {
                addLineCoverageToMap(previousLine, fileCoverageMap)
            }
            previousLine = line
        }
        if (previousLine != null) {
            addLineCoverageToMap(previousLine, fileCoverageMap)
        }

        var path = Paths.get(file.path)
        if (path.isAbsolute) {
            path = projectPath.relativize(path);
        }
        return convertFileCoverage(path.toString(), fileCoverageMap)
    }

    fun addLineCoverageToMap(line: Line, fileCoverageMap: HashMap<String, MutableSet<String>>) {
        val coverageType = convertLineCoverage(line)
        fileCoverageMap[coverageType]!!.add(line.num)
    }

    fun convertLineCoverage(line: Line): String =
            if (line.count == "0") {
                UNCOVERED_KEY
            } else if (line.type == "cond") {
                convertConditionCoverage(line)
            } else {
                COVERED_KEY
            }

    fun convertConditionCoverage(conditionCoverage: Line): String =
            if (conditionCoverage.truecount == "0" && conditionCoverage.falsecount == "0") {
                UNCOVERED_KEY
            } else if (conditionCoverage.truecount == "0" || conditionCoverage.falsecount == "0") {
                PARTLY_COVERED_KEY
            } else {
                COVERED_KEY
            }

}
