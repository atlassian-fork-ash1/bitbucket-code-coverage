package com.atlassian.bitbucket.coverage

import org.xml.sax.InputSource
import java.io.File
import java.io.IOException
import javax.xml.bind.JAXBContext
import javax.xml.bind.JAXBException
import javax.xml.parsers.SAXParserFactory
import javax.xml.transform.sax.SAXSource

class XmlReader {
    @Throws(JAXBException::class, IOException::class)
    fun <T> read(clazz: Class<T>, xml: File): T {
        val jc = JAXBContext.newInstance(clazz)
        val u = jc.createUnmarshaller()
        // U can use IOUtils.toInputStream(xml) if add to deps commons-io and parse String
        // Also, it can be Node, String, etc

        val spf = SAXParserFactory.newInstance()
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
        spf.setFeature("http://xml.org/sax/features/validation", false)

        val reader = spf.newSAXParser().xmlReader
        val inputSource = InputSource(xml.toURL().openStream())

        return u.unmarshal(SAXSource(reader, inputSource), clazz).value
    }

}
