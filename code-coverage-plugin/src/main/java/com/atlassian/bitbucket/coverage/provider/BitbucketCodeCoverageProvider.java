package com.atlassian.bitbucket.coverage.provider;

import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverage;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageCallback;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageLevel;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageProvider;
import com.atlassian.bitbucket.codeinsights.coverage.CodeCoverageRequest;
import com.atlassian.bitbucket.coverage.CoverageManager;
import com.atlassian.bitbucket.coverage.CoverageParser;
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity;
import com.atlassian.bitbucket.pull.PullRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.PARTLY_COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.UNCOVERED_COLUMN;
import static java.util.Objects.requireNonNull;

@Component
public class BitbucketCodeCoverageProvider implements CodeCoverageProvider {

    static final String PROVIDER_KEY = "bitbucket-code-coverage-provider";
    static final String PROVIDER_URL = "";

    private final CoverageManager coverageManager;
    private final CoverageParser coverageParser;

    @Autowired
    public BitbucketCodeCoverageProvider(CoverageManager coverageManager, CoverageParser coverageParser) {
        this.coverageManager = coverageManager;
        this.coverageParser = coverageParser;
    }

    private static CodeCoverage coverage(int start, int end, CodeCoverageLevel level) {
        return new CodeCoverage() {
            @Nonnull
            @Override
            public CodeCoverageLevel getCodeCoverageLevel() {
                return level;
            }

            @Override
            public int getEnd() {
                return end;
            }

            @Nonnull
            @Override
            public Optional<String> getMessage() {
                return Optional.empty();
            }

            @Override
            public int getStart() {
                return start;
            }
        };
    }

    @Override
    public void streamCoverage(@Nonnull CodeCoverageRequest codeCoverageRequest,
                               @Nonnull CodeCoverageCallback codeCoverageCallback) {
        requireNonNull(codeCoverageRequest, "codeCoverageRequest");
        requireNonNull(codeCoverageCallback, "codeCoverageCallback");

        PullRequest pullRequest = codeCoverageRequest.getPullRequest();
        String commitId = pullRequest.getFromRef().getLatestCommit();
        String path = codeCoverageRequest.getPath();

        FileCoverageEntity result = coverageManager.getCoverage(commitId, path);
        addCoverage(result, codeCoverageCallback);
    }

    private void addCoverage(FileCoverageEntity coverageEntity, CodeCoverageCallback callback) {
        if (coverageEntity == null) {
            return;
        }

        callback.onFileStart(PROVIDER_KEY, PROVIDER_URL, coverageEntity.getPath());

        Map<String, Set<String>> coverage = coverageParser.parseCoverage(coverageEntity.getCoverage());
        addCoverageRanges(coverage.get(COVERED_COLUMN), CodeCoverageLevel.FULL, callback);
        addCoverageRanges(coverage.get(PARTLY_COVERED_COLUMN), CodeCoverageLevel.PARTIAL, callback);
        addCoverageRanges(coverage.get(UNCOVERED_COLUMN), CodeCoverageLevel.NONE, callback);

        callback.onFileEnd();
    }

    private void addCoverageRanges(Set<String> coverage, CodeCoverageLevel level, CodeCoverageCallback callback) {
        if (coverage.isEmpty()) {
            return;
        }

        // Aggregate consecutive line numbers.
        // For example, if we have full coverage for lines "2,3,4,5,6" we add it as coverage on lines "2-6".
        List<Integer> lines = coverage.stream().map(Integer::valueOf).sorted().collect(Collectors.toList());
        int low = lines.get(0), high = low;
        for (int next : lines) {
            if (next > high + 1) {
                callback.onRange(coverage(low, high, level));
                low = next;
            }
            high = next;
        }
        callback.onRange(coverage(low, high, level));
    }
}
