package com.atlassian.bitbucket.coverage.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bitbucket.coverage.CoverageMerger;
import com.atlassian.bitbucket.coverage.CoverageParser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.COMMIT_ID_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.PARTLY_COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.PATH_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.UNCOVERED_COLUMN;

@Component
public class AoCodeCoverageDao implements CodeCoverageDao {
    private static final String ID_AND_PATH_QUERY =
            String.format("%s = ? AND %s = ?", COMMIT_ID_COLUMN, PATH_COLUMN);
    private static final String ID_QUERY = String.format("%s = ?", COMMIT_ID_COLUMN);

    private static final Logger log = LoggerFactory.getLogger(AoCodeCoverageDao.class);

    private final ActiveObjects ao;
    private final CoverageParser coverageParser;
    private final CoverageMerger coverageMerger;

    @Autowired
    public AoCodeCoverageDao(
            @ComponentImport ActiveObjects ao,
            CoverageParser coverageParser,
            CoverageMerger coverageMerger) {
        this.ao = ao;
        this.coverageParser = coverageParser;
        this.coverageMerger = coverageMerger;
    }

    public AoFileCoverage addFileCoverage(String commitId, String path, String coverage) {
        AoFileCoverage fileCoverage = findFileCoverage(commitId, path);

        if (null == fileCoverage) {
            fileCoverage = createFileCoverage(commitId, path, coverage);
        } else {
            fileCoverage = mergeFileCoverage(fileCoverage, coverage);
        }

        return fileCoverage;
    }

    public int deleteCoverageForCommit(String commitId) {
        int result = ao.deleteWithSQL(AoFileCoverage.class, ID_QUERY, commitId);
        log.info("{} coverage entries were deleted for commit {}", result, commitId);
        return result;
    }

    public AoFileCoverage findFileCoverage(String commitId, String path) {
        AoFileCoverage[] foundEntries = ao.find(AoFileCoverage.class, ID_AND_PATH_QUERY, commitId, path);
        if (foundEntries.length == 0) {
            return null;
        }
        return foundEntries[0];
    }

    public AoFileCoverage mergeFileCoverage(AoFileCoverage fileCoverage, String coverage) {
        Map<String, Set<String>> oldCoverage = coverageParser.parseCoverage(
                fileCoverage.getCovered(), fileCoverage.getPartlyCovered(), fileCoverage.getUnCovered());
        Map<String, Set<String>> newCoverage = coverageParser.parseCoverage(coverage);

        Map<String, Set<String>> mergedCoverage = coverageMerger.mergeCoverage(oldCoverage, newCoverage);
        return updateFileCoverage(fileCoverage, mergedCoverage);
    }

    private AoFileCoverage updateFileCoverage(AoFileCoverage fileCoverage, Map<String, Set<String>> coverageMap) {
        fileCoverage.setCovered(coverageParser.linesToCoverage(coverageMap.get(COVERED_COLUMN)));
        fileCoverage.setPartlyCovered(coverageParser.linesToCoverage(coverageMap.get(PARTLY_COVERED_COLUMN)));
        fileCoverage.setUnCovered(coverageParser.linesToCoverage(coverageMap.get(UNCOVERED_COLUMN)));

        fileCoverage.save();
        return fileCoverage;
    }

    private AoFileCoverage createFileCoverage(String commitId, String path, String coverage) {
        ImmutableMap.Builder<String, Object> paramBuilder = ImmutableMap.<String, Object>builder()
                .put(COMMIT_ID_COLUMN, commitId)
                .put(PATH_COLUMN, path);
        Map<String, Set<String>> coverageMap = coverageParser.parseCoverage(coverage);

        paramBuilder.put(COVERED_COLUMN, coverageParser.linesToCoverage(coverageMap.get(COVERED_COLUMN)));
        paramBuilder.put(PARTLY_COVERED_COLUMN, coverageParser.linesToCoverage(coverageMap.get(PARTLY_COVERED_COLUMN)));
        paramBuilder.put(UNCOVERED_COLUMN, coverageParser.linesToCoverage(coverageMap.get(UNCOVERED_COLUMN)));

        return ao.create(AoFileCoverage.class, paramBuilder.build());
    }

}
