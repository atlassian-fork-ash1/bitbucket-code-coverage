package com.atlassian.bitbucket.coverage.rest;

import com.atlassian.bitbucket.coverage.CoverageManager;
import com.atlassian.bitbucket.coverage.InvalidCoverageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

@Path("commits/{commitId}")
@Produces({APPLICATION_JSON})
@Consumes({APPLICATION_JSON})
@Component
public class CodeCoverageResource {
    private final CoverageManager coverageManager;

    @Autowired
    public CodeCoverageResource(CoverageManager coverageManager) {
        this.coverageManager = coverageManager;
    }

    @POST
    public Response postCoverage(@PathParam("commitId") String commitId, CommitCoverageEntity commitCoverageEntity) {
        if (null == commitCoverageEntity) {
            return status(BAD_REQUEST).entity("Body may not be empty").build();
        }
        try {
            List<FileCoverageEntity> result = coverageManager.saveCoverage(commitId, commitCoverageEntity);
            return ok(result).build();
        } catch (InvalidCoverageException e) {
            return status(BAD_REQUEST).entity(new InvalidCoverageMessage(e)).build();
        }
    }

    @GET
    public Response getCoverage(@PathParam("commitId") String commitId, @QueryParam("path") String path) {
        if (null == path) {
            return status(BAD_REQUEST).entity("Missing required field: path").build();
        }
        FileCoverageEntity result = coverageManager.getCoverage(commitId, path);
        return ok(result).build();
    }

    @DELETE
    public Response deleteCoverage(@PathParam("commitId") String commitId) {
        int deleted = coverageManager.deleteCoverage(commitId);
        return ok(String.format("%s entries deleted", deleted)).build();
    }

}
